###############################################################################
# .zshrc config
###############################################################################

[[ $- != *i* ]] && return

unsetopt beep

if [[ -z "$ZSH_CACHE_DIR" ]]; then
  ZSH_CACHE_DIR="$XDG_CACHE_HOME/zsh"
fi

if [[ -z "$ZSH_CONFIG_LIB" ]]; then
  ZSH_CONFIG_LIB="${ZDOTDIR}/lib"
fi

autoload -U compaudit compinit zrecompile

setopt auto_cd
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushdminus

setopt NO_CASE_GLOB
setopt EXTENDED_GLOB
setopt NUMERIC_GLOB_SORT

###############################################################################
# autocomplete settings
###############################################################################

if [[ "$OSTYPE" = darwin* ]]; then
    SHORT_HOST=$(scutil --get ComputerName 2>/dev/null) || SHORT_HOST=${HOST/.*/}
else
    SHORT_HOST=${HOST/.*/}
fi

# Save the location of the current completion dump file.
if [[ -z "$ZSH_COMPDUMP" ]]; then
  ZSH_COMPDUMP="${ZSH_CACHE_DIR}/.zcompdump-${SHORT_HOST}-${ZSH_VERSION}"
fi

if [[ "$ZSH_DISABLE_COMPFIX" != true ]]; then
  source "$ZDOTDIR/lib/compfix.zsh"
  # Load only from secure directories
  compinit -i -d "$ZSH_COMPDUMP"
  # If completion insecurities exist, warn the user
  handle_completion_insecurities &|
else
  # If the user wants it, load from all found directories
  compinit -u -d "$ZSH_COMPDUMP"
fi

zstyle ':completion:*' menu select
setopt COMPLETE_IN_WORD
setopt ALWAYS_TO_END
setopt MENU_COMPLETE
setopt COMPLETE_ALIASES
setopt LIST_ROWS_FIRST
setopt CORRECT_ALL

###############################################################################
# keybinding settings
###############################################################################

bindkey -v
#bindkey -M menuselect 'h' vi-backward-char
#bindkey -M menuselect 'j' vi-down-line-or-history
#bindkey -M menuselect 'k' vi-up-line-or-history
#bindkey -M menuselect 'l' vi-forward-char
#bindkey -M menuselect 'left' vi-backward-char
#bindkey -M menuselect 'down' vi-down-line-or-history
#bindkey -M menuselect 'up' vi-up-line-or-history
#bindkey -M menuselect 'right' vi-forward-char

###############################################################################
# sourced plugin files
###############################################################################

[ -f "${ZSH_CONFIG_LIB}/aliases.zsh" ] && source "${ZSH_CONFIG_LIB}/aliases.zsh"
[ -f "${ZSH_CONFIG_LIB}/functions.zsh" ] && source "${ZSH_CONFIG_LIB}/functions.zsh"
[ -f "${ZSH_CONFIG_LIB}/history.zsh" ] && source "${ZSH_CONFIG_LIB}/history.zsh"
[ -f "${ZSH_CONFIG_LIB}/theme.zsh" ] && source "${ZSH_CONFIG_LIB}/theme.zsh"

#source ${ZDOTDIR}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#source ${ZDOTDIR}/zsh-autosuggestions/zsh-autosuggestions.zsh

[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"

# set completion colors to be the same as `ls`, after theme has been loaded
[[ -z "$LS_COLORS" ]] || zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

###############################################################################
# zsh prompt theme
###############################################################################

autoload -U colors && colors
autoload -Uz vcs_info && precmd () { vcs_info }
setopt PROMPT_SUBST
zstyle ':vcs_info:*' formats '%b'
autoload -U add-zsh-hook
add-zsh-hook precmd main_prompt
