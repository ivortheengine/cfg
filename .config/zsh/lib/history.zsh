###############################################################################
# zsh history settings
###############################################################################

HISTFILE="${ZSH_CACHE_DIR}/.zhistory"
HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd ..)"
[ "$HISTSIZE" -lt 50000 ] && HISTSIZE=50000
[ "$SAVEHIST" -lt 10000 ] && SAVEHIST=10000

bindkey "^R" history-incremental-pattern-search-backward
setopt EXTENDED_HISTORY
setopt APPEND_HISTORY
setopt HIST_FIND_NO_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_SPACE
setopt HIST_VERIFY
setopt SHARE_HISTORY
setopt HIST_IGNORE_DUPS
setopt INC_APPEND_HISTORY
setopt HIST_REDUCE_BLANKS
