function main_prompt {
    if [[ -n $SSH_CLIENT ]]; then
        ssh="%{%F{blue}%}●%{%f%}"
        # check for network or just print ip or host
    fi
    env=""
    if [[ -n $VIRTUAL_ENV ]]; then
        env='('`basename $VIRTUAL_ENV`') '
    fi
    current_dir=$(git rev-parse --is-inside-work-tree 2>&1)
    # check if current dir is a git folder
    if [[ ${current_dir} == true ]]; then
    	STATUS="$(git status 2>/dev/null)"
	# get current git status
        while read -r -A Line; do
	    if [[ ${Line[1]}${Line[2]}${Line[3]} == \(fixconflictsand ]]; then
                git_status="%{%F{red}%}!%{%f%}"
                break
            elif [[ ${Line[1]}${Line[2]} == Untrackedfiles: ]]; then
                git_status="%{%F{red}%}●%{%f%}"
                break
            elif [[ ${Line[1]} == deleted: ]]; then
                git_status="%{%F{red}%}●%{%f%}"
                break
            elif [[ ${Line[1]} == "modified:" ]]; then
                git_status="%{%F{red}%}●%{%f%}"
                break
            elif [[ ${Line[1]}${Line[2]}${Line[3]}${Line[4]} == Changestobecommitted: ]]; then
                git_status="%{%F{red}%}%{%f%}"
                break
            elif [[ ${Line[1]}${Line[2]}${Line[4]} == Yourbranchahead ]]; then
                git_status="%{%F{red}%}%{%f%}"
                break
            elif [[ ${Line[1]}${Line[2]}${Line[3]} == nothingtocommit, ]]; then
                git_status=""
                break
	    fi
        done <<<${STATUS}

        PROMPT='${ssh} ${env}%2~ [ ${vcs_info_msg_0_} ${git_status}]»%b '
    else
        PROMPT='${ssh} ${env}%2~ »%b '
    fi
    PS1=${PROMPT}
}

