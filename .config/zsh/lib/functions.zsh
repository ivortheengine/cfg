ex() {
  if [ -f $1 ]; then
    case $1 in
    *.tar.bz2) tar xjf $1 ;;
    *.tar.gz) tar xzf $1 ;;
    *.bz2) bunzip2 $1 ;;
    *.rar) unrar x $1 ;;
    *.gz) gunzip $1 ;;
    *.tar) tar xf $1 ;;
    *.tbz2) tar xjf $1 ;;
    *.tgz) tar xzf $1 ;;
    *.zip) unzip $1 ;;
    *.Z) uncompress $1 ;;
    *.7z) 7za e x $1 ;;
    *.deb) ar x $1 ;;
    *.tar.xz) tar xf $1 ;;
    *.tar.zst) unzstd $1 ;;
    *) echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# fzf find directory
ffd() {
  local pre="tree -L 1{}"
  local bind="space:toggle-preview"
  cd $HOME
  cd "$(fd -t d | fzf --preview=${pre} \
    --bind=${bind} \
    --reverse \
    --preview-window=:hidden)"
}
# fzf find open file
ffof() {
  local pre="xdg-mime query default {}"
  fd -t f -H -I | fzf -m --preview=${pre} | xargs -ro -d "\n" xdg-open 2>&-
}

# fzf find node package
ffnp() {
  local script
  script=$(cat package.json | jq -r '.scripts | keys[] ' | sort | fzf)
  npm run $(echo "$script")
}

# fzf brew install
fbi() {
  local inst=$(brew search "$@" | fzf -m)

  if [[ $inst ]]; then
    for prog in $(echo $inst);
    do; brew install $prog; done;
  fi
}

# fzf brew delete
fbd() {
  local uninst=$(brew leaves | fzf -m)

  if [[ $uninst ]]; then
    for prog in $(echo $uninst);
    do; brew uninstall $prog; done;
  fi
}
