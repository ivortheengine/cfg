###############################################################################
# aliases
###############################################################################

alias dotconfig="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
# alias dotconfig dotconfig --local status.showUntrackedFiles no

alias sinfo="$HOME/.config/pfetch/pfetch-debian"

alias md="mkdir -p"
alias rd="rmdir"

alias tmux="tmux -u"
alias wget="wget --hsts-file='$XDG_CACHE_HOME/wget-hsts'"

alias git_delete_merged="git branch --merged | egrep -v '(^\*|master|main|dev|production|next)$' | xargs git branch -d"
alias git_submodule_pull="git submodule sync --recursive && git submodule update --init --recursive"

alias docker_scan_crit="docker run --rm -v /tmp/trivy:/root/.cache -v /var/run/docker.sock:/var/run/docker.sock -it aquasec/trivy:latest image --offline-scan --severity CRITICAL"
alias docker_scan_severity="docker run --rm -v /tmp/trivy:/root/.cache -v /var/run/docker.sock:/var/run/docker.sock -it aquasec/trivy:latest image --offline-scan --severity $1"
alias docker_scan="docker run --rm -v /tmp/trivy:/root/.cache -v /var/run/docker.sock:/var/run/docker.sock -it aquasec/trivy:latest image --offline-scan"

