#! /usr/bin/env bash

###############################################################################
# bashrc config 
###############################################################################

[[ $- != *i* ]] && return

[[ -z "$XDG_CONFIG_HOME" ]] && export XDG_CONFIG_HOME="$HOME/.config/"
[[ -z "$XDG_DATA_HOME" ]] && export XDG_DATA_HOME="$HOME/.local/share/"
[[ -z "$XDG_CACHE_HOME" ]] && export XDG_CACHE_HOME="$HOME/.cache/"

function main_prompt {
    # colours
    local c_red="\e[31m"
    local c_green="\e[32m"
    local c_white="\e[37m"
    local c_b_red="\e[1;31m"
    local c_b_green="\e[1;32m"
    local reset="\e[0m"

    current_dir=`git rev-parse --is-inside-work-tree 2>&1`
    # check if current dir is a git folder
    if [[ ${current_dir} == true ]]; then
        Top=`git rev-parse --show-toplevel 2>&1`
        # get current git branch
        if [[ -n ${Top} ]]; then
            git_branch=`git rev-parse --abbrev-ref HEAD`
        fi
        STATUS="$(git status 2> /dev/null)"
        # get current git status
        while read -ra Line; do
            if [[ ${Line[0]}${Line[1]}${Line[2]} == \(fixconflictsand ]]; then
                git_status="${c_red}!${reset}"
                break
            elif [[ ${Line[0]}${Line[1]} == Untrackedfiles: ]]; then
                git_status="${c_red}●${reset}"
                break
            elif [[ ${Line[0]} == deleted: ]]; then
                git_status="${c_red}●${reset}"
                break
            elif [[ ${Line[0]} == modified: ]]; then
                git_status="${c_red}●${reset}"
                break
            elif [[ ${Line[0]}${Line[1]}${Line[2]}${Line[3]} == Changestobecommitted: ]]; then
                git_status="${c_red}${reset}"
                break
            elif [[ ${Line[0]}${Line[1]}${Line[3]} == Yourbranchahead ]]; then
                git_status="${c_red}${reset} "
                break
            elif [[ ${Line[0]}${Line[1]}${Line[2]} == nothingtocommit, ]]; then
                git_status=""
                break
            fi
        done <<< ${STATUS}

        PROMPT="\w [ ${git_branch} ${git_status}]» "
    else
        PROMPT="\w » "
    fi
    PS1=${PROMPT}
}

PROMPT_COMMAND='main_prompt'

export EDITOR="nvim"
export TERM="xterm-256color"
export TIMEFORMAT="%3R"
export HISTCONTROL="ignoreboth"
export HISTFILE="$HOME/.cache/.bash_history"
export HISTSIZE=8000
export HISTTIMEFORMAT="[%F (%X)]: " 

export LESSHISTFILE="${XDG_CACHE_HOME}/less/history"
export LS_COLORS="di=0;34:"

# Pretty-print man(1) pages.
export LESS_TERMCAP_mb=$'\e[1;91m'
export LESS_TERMCAP_md=$'\e[1;91m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_so=$'\e[1;93m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;92m'

[ -n "$BASH_VERSINFO" ] || return 
((BASH_VERSINFO[0] >= 3)) || return

if ((BASH_VERSINFO[0] >= 4)) ; then
    shopt -s dirspell globstar
    if ((BASH_VERSINFO[1] >= 3)) ; then
        shopt -s direxpand
    fi
fi

shopt -s checkwinsize complete_fullquote dotglob extquote extglob \
	     force_fignore hostcomplete xpg_echo promptvars sourcepath progcomp \
         autocd cdspell nocasematch histappend cmdhist lithist

stty stop ''

set -o braceexpand -o hashall -o histexpand -o vi

[[ -d "$HOME/.bin" ]] && PATH="$HOME/.bin:$PATH"

[[ -d "$HOME/.local/.bin" ]] && PATH="$HOME/.local/bin:$PATH"

[[ -d "$HOME/.programs" ]] && PATH="$HOME/.programs:$PATH"

aliases="$HOME/.config/bash/.bash_aliases"
[[ -f $aliases && -r $aliases ]] && . "$aliases"

completion_dir="/usr/share/bash-completion/bash_completion"
[[ -f ${completion_dir} && -r $completion_dir ]] && . "$completion_dir"

functions="$HOME/.config/bash/.bash_functions"
[[ -f $functions && -r $functions ]] && . "$functions"

