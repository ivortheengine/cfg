#!/bin/sh

alias v="$EDITOR"
alias vim="$EDITOR"
alias ls="ls --color=auto"
alias ufetch="bash $HOME/.config/ufetch/ufetch-debian"

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
