#! /bin/sh/

###############################################################################
# debian installation script 
###############################################################################

# https://github.com/sudorook/debian
# https://github.com/Calchan/debian-install-scripts/blob/main/install

###############################################################################
# disk layout: plain dm-crypt
# see: https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Plain_dm-crypt 
# 
# -----------------------------------------------------------------------------
# log volume 1     | log volume 2     | log volume 3     | 
# root             | swap             | home             | boot + encrypt key   
#                  |                  |                  |
# -----------------------------------------------------------------------------
# /dev/sda: encrypted using plain mode on lvm.           | USB 
# -----------------------------------------------------------------------------
#
###############################################################################

# global variables
release="bookworm (testing)"
device="sda" 
usb_device="sdb"
root="32GB"
swap="10GB"
volgroup="VolGroup"
hostname="office-pc"
username="user"
locale="en_UK.UTF-8"
charset="UTF-8"
timezone="UTC"
kblayout="gb"

function disk_setup {
    
    # create boot partition

    # create keyfile 
    dd bs=256 count=32 if=/dev/urandom of="/dev/${usb_device}/keyfile" iflag=fullblock

    # remove current partition
    sgdisk --zap-all "/dev/${device}" && partprobe "/dev/${device}"     

    # secure erase disk
    cryptsetup open --type plain -d /dev/urandom "/dev/${device}" to_be_wiped
    dd if=/dev/zero of="/dev/${device}/to_be_wiped" status=progress bs=1M
    cryptsetup close to_be_wiped   

    # encrypting disk plain mode 
    cryptsetup --cipher=aes-xts-plain64 --offset=0 --key-file="/dev/${usb_device}" --key-size=512 \
        open --type plain "/dev/${device}" cryptlvm

    # LVM
    pvcreate /dev/mapper/cryptlvm
    vgcreate ${vg} /dev/mapper/cryptlvm
    lvcreate -L ${root} ${vg} -n root
    lvcreate -L ${swap} ${vg} -n swap
    lvcreate -l 100%FREE ${vg} -n home

    # format volumes
    mkfs.ext4 /dev/${vg}/root
    mkfs.ext4 /dev/${vg}/home
    
    # format swap
    mkswap /dev/${vg}/swap
}

function base_install {
    
    # install base system
    debootstrap --arch amd64 ${release} /mnt https://deb.debian.org/debian \
    # to add

    mkdir -p /mnt/run/udev

    for f in [proc, sys, dev, run]; do
        mount --bind /$f /mnt/$f
    done
    
}

function main {
    
    # partition drives
    disk_setup

    # mount volumes
    mount /dev/${vg}/root /mnt
    mkdir -p /mnt/home
    mount /dev/${vg}/home /mnt/home

    # base install
    base_install

    # chroot in installed base system
    chroot /mnt /bin/bash

    # stopped there ...
    # note need cryptsetup installed before disk setup

    # add grub install

    # set locals
    echo "${locale} ${charset}" >> /etc/locale.gen
    locale.gen
    echo "LANG=${locale}" > /etc/locale.conf
    echo "KEYMAP=${locale}" > /etc/vconsole.conf
    
    # set keyboard
    sed -i "s/^\(XKBLAYOUT=\)\".*\"/\1\"${KBLAYOUT}\"/g" /etc/default/keyboard
    
    # set timezone
    ln -sfn "/usr/share/zoneinfo/${timezone}" /etc/localtime
    hwclock --systohc --utc
    
    # set hostname
    echo "${hostname}" > /etc/hostname

    # unmount 
    umount -R /mnt 
}


