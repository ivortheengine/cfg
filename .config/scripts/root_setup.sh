#! /bin/sh

# update source list
rm /etc/apt/source.list
touch /etc/apt/source.list

sources = [ \
    "deb https://deb.debian.org/debian               testing          main contrib non-free", \
    "deb-src https://deb.debian.org/debian           testing          main contrib non-free", \
    "deb https://deb.debian.org/debian-security/     testing-security main contrib non-free", \
    "deb-src https://deb.debian.org/debian-security/ testing-security main contrib non-free", \
    "deb https://deb.debian.org/debian               testing-updates  main contrib non-free", \
    "deb-src https://deb.debian.org/debian           testing-updates  main contrib non-free", \
]

for line in sources ; do
    echo "$line" >> /etc/apt/source.list 
done
 
# update packages
apt-get update && apt-get upgrade -y
apt-get autoremove && apt-get autoclean 

# install sudo
apt-get install sudo -y

# adding standard user to sudoers file
user=$(getent passwd 1000 |  awk -F: '{ print $1}')
echo "$user ALL=(ALL:ALL) ALL" >> /etc/sudoers

