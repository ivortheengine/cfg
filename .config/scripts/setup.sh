#! /bin/sh

function install_nerd_font {
	font_name="$1"
	repo="https://github.com/ryanoasis/nerd-fonts/releases/tag/v2.2.2"
	cmd="wget --secure-protocol=tlsv1_3"

	$cmd "${repo}/$1"
	unzip "${1}.zip"
	mkdir -p "HOME/.local/share/fonts/nerdfonts/${1}"
	mv *.tff "HOME/.local/share/fonts/nerdfonts/${1}"
	mv *.off "HOME/.local/share/fonts/nerdfonts/${1}"
	rm "${1}.zip" readme.md README.md LICENSE.txt
	
	fc-cache -f -v
}

sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get auto-purge && sudo apt-get autoclean

# installing nerd fonts
nerd_fonts=( Inconsolata Iosevka SauceCodePro )
for font_name in $nerd_fonts; do
	repo="https://github.com/ryanoasis/nerd-fonts/releases/tag/v2.2.2"
	cmd="wget --secure-protocol=tlsv1_3"
	$cmd "${repo}/${font_name}"
	unzip "${font_name}.zip"
	mkdir -p "HOME/.local/share/fonts/nerdfonts/${font_name}"
	mv *.tff "HOME/.local/share/fonts/nerdfonts/${font_name}"
	mv *.off "HOME/.local/share/fonts/nerdfonts/${font_name}"
	rm "${font_name}.zip" readme.md README.md LICENSE.txt
	
	fc-cache -f -v
done

