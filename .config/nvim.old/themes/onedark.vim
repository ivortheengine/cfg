""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" theme 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set background=dark
syntax on
colorscheme onedark

let g:onedark_termcolors=256

let g:lightline = { "colorscheme": "onedark" }
