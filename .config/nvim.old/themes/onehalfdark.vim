""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" theme 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set t_Co=256
colorscheme onehalfdark 
let g:airline_theme='onehalfdark'
let g:lightline = { 'colorscheme': 'onehalfdark' }
