""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" neovim v4.3 config file
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

source ~/.config/nvim/vim-plug/plugins.vim
source ~/.config/nvim/general/settings.vim
source ~/.config/nvim/general/keybinds.vim
source ~/.config/nvim/themes/onehalfdark.vim
source ~/.config/nvim/vim-plug/coc.vim
source ~/.config/nvim/vim-plug/fzf.vim
