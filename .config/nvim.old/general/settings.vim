"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" leader
let g:mapleader="\<Space>"

" general
set mouse         =a
set cursorline
set clipboard     =unnamedplus
set encoding      =utf-8
set fileencoding  =utf-8
set t_Co          =256

set updatetime    =300
set noshowmode

" layout
set splitbelow
set splitright
set cmdheight     =2
set pumheight     =10

" backups and swap
set nobackup
set noswapfile
set nowb
set undofile
set undodir       =~/.cache/nvimundo

" indentaion and spaces  
set relativenumber
set nowrap
set signcolumn    =yes
set tabstop       =4
set softtabstop   =4
set shiftwidth    =4
set colorcolumn   =80
:highlight ColorColumn ctermbg=1
set scrolloff     =8
set sidescrolloff =8
set expandtab
set autoindent

" search
set hlsearch
set ignorecase
set smartcase
set iskeyword+=-

au! BufWritePost ~/.config/nvim/init.vim source %
