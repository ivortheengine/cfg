"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" fzf
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:fzf_action = { "ctrl-v": "vsplit" }

let g:fzf_historty_dir="~/.cache/nvim-fzf-history"

map <leader>ff :Files<CR>
nnoremap <leader>fg :Rg<CR>
