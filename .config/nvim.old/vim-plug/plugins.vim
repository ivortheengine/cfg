"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" install vim-plug if not found
if empty(glob("~/.local/share/nvim/site/autoload/plug.vim"))
    silent !curl -flo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" run PlugInstall if any missing plugins
autocmd VimEnter *
    \ if len(filter(values(g:plugs), "!isdirectory(v:val.dir)")) 
    \| PlugInstall --sync | source "~/.config/nvim/init.vim"
    \| endif

" defining plugins
call plug#begin("~/.local/share/nvim/site/autoload/plugged")

    Plug 'airblade/vim-rooter'
    Plug 'itchyny/lightline.vim'
    Plug 'jiangmiao/auto-pairs'
    Plug 'joshdick/onedark.vim'
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'junegunn/gv.vim'
    Plug 'mhinz/vim-signify' 
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'sheerun/vim-polyglot'
    Plug 'sonph/onehalf', { 'rtp': 'vim' }
    Plug 'tpope/vim-fugitive' 
    Plug 'tpope/vim-rhubarb' 

call plug#end()

let g:coc_disable_startup_warning = 1

" auto install pluggins on start up
autocmd VimEnter *
    \ if len(filter(values(g:plugs), "!isdirectory(v:val.dir)"))
    \| PlugInstall --sync | source "~/.local/share/nvim/init.vim"
    \| endif
