local status_ok, mason = pcall(require, "mason")
if not status_ok then
  return
end

mason.setup({
    ui = {
	    border = "none",
		check_outdated_packages_on_open = true,
        icons = {
			package_installed = "◍",
            package_pending = "◍",
            package_uninstalled = "◍"
        },
    },
    keymaps = {
        toggle_package_expand = "<CR>",
        install_package = "i",
        update_package = "u",
        check_package_version = "c",
        update_all_packages = "U",
        check_outdated_packages = "C",
        uninstall_package = "xx",
        cancel_installation = "<C-c>",
        apply_language_filter = "<C-f>",
    },
	log_level = vim.log.levels.INFO,
	max_concurrent_installers = 4,
    providers = {
        "mason.providers.registry-api",
    },
    pip = {
        upgrade_pip = true,
    }
})
