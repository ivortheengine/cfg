local status_ok, theme = pcall(require, "onedark")
if not status_ok then
  return
end

theme.setup {
    style = 'warmer',
    term_colors = true,

    code_style = {
        comments = 'none',
        keywords = 'none',
        functions = 'none',
        strings = 'none',
        variables = 'none'
    },

}

theme.load()
