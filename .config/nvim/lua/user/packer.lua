PROMPT_COMMAND='main_prompt'
-- Automatically install packer
local fn = vim.fn local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

-- Install your plugins here
return packer.startup(function(use)
	use "wbthomason/packer.nvim"

    -- telescope
	use {"nvim-telescope/telescope.nvim", tag = "0.1.0",
		requires = { {"nvim-lua/plenary.nvim"} }
	}

	-- colour scheme and syntax highlighting
	use 'navarasu/onedark.nvim'
	use {'nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'}}

    -- lualine
    use {'nvim-lualine/lualine.nvim',
        requires = { 'nvim-tree/nvim-web-devicons', opt = true }
    }

    -- tree directory
    use {'nvim-tree/nvim-tree.lua',
        requires = {'nvim-tree/nvim-web-devicons'},
        tag = 'nightly'
    }

    -- toggle terminal
    use {"akinsho/toggleterm.nvim", tag = '*',
        config = function() require("toggleterm").setup() end
    }

    -- autopairs
    use "windwp/nvim-autopairs"

    -- line indentation
    use "lukas-reineke/indent-blankline.nvim"

    -- git
    use "lewis6991/gitsigns.nvim"

    -- commenting
    use {'numToStr/Comment.nvim',
        config = function() require('Comment').setup() end
    }
    use "JoosepAlviste/nvim-ts-context-commentstring"

    -- key binding popup
    use 'folke/which-key.nvim'

    -- start screen
    -- use 'goolord/alpha-nvim'

    use { 'VonHeikemen/lsp-zero.nvim',
        requires = {
            -- LSP Support
            use 'neovim/nvim-lspconfig',
            use 'williamboman/mason.nvim',
            use 'williamboman/mason-lspconfig.nvim',
            use 'jose-elias-alvarez/null-ls.nvim',
            use 'RRethy/vim-illuminate',

            -- Autocompletion
            use 'hrsh7th/nvim-cmp',
            use 'hrsh7th/cmp-buffer',
            use 'hrsh7th/cmp-path',
            use 'saadparwaiz1/cmp_luasnip',
            use 'hrsh7th/cmp-nvim-lsp',
            use 'hrsh7th/cmp-nvim-lua',

            -- Snippets
            use 'L3MON4D3/LuaSnip',
            -- Snippet Collection (Optional)
            --{'rafamadriz/friendly-snippets'},
        },
    }

  	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
