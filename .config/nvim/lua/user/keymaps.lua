vim.g.mapleader=" "
vim.g.localmapleader=" "

local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

-- insert
keymap("i", "kj", "<Esc>", opts)

-- normal
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- visual
vim.keymap.set("v", "kj", "<Esc>", opts)


