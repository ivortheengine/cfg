###############################################################################
# .zshenv config
###############################################################################

export LC_ALL="en_UK.UTF-8"
export LANG="en_UK.UTF-8"

export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

export BROWSER="Firefox"
export EDITOR="vim"
export TERM="xterm-256color"
export TERMINAL="alacritty"
export VISUAL="nvim"

if [[ $(uname) == "Darwin" ]]; then
    export TERMINAL="iTerm2"
fi

export CLICOLOR=1
export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd

export AWS_CONFIG_FILE="${XDG_CONFIG_HOME}/aws"
export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export NVM_DIR="$XDG_DATA_HOME"/nvm
export PYTHONPATH="/usr/bin/python3"
export PYTHONPYCACHEPREFIX=$XDG_CACHE_HOME/python
export PYTHONUSERBASE=$XDG_DATA_HOME/python
export TERMINFO="$XDG_DATA_HOME"/terminfo
export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo
export VAGRANT_HOME="$XDG_DATA_HOME"/vagrant
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME"/vagrant/aliases
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
